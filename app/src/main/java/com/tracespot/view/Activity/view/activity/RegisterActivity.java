package com.tracespot.view.Activity.view.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.User;
import com.tracespot.view.Activity.model.mPreferences;

import net.yazeed44.imagepicker.util.Picker;

import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements Firebase.ValueResultHandler<Map<String, Object>> {


    private EditText etxt_email;
    private EditText etxt_name;
    private EditText etxt_age;
    private EditText etxt_phone;
    private EditText etxt_password;
    private TextView txt_image64;
    private ImageView image_select;
    private User newUser;
    private Firebase firebase;
    private ImageController ic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Set de los controles
        etxt_email = (EditText) findViewById(R.id.etxt_email);
        etxt_name = (EditText) findViewById(R.id.etxt_name);
        etxt_age = (EditText) findViewById(R.id.etxt_age);
        etxt_phone = (EditText) findViewById(R.id.etxt_phone);
        etxt_password = (EditText) findViewById(R.id.etxt_password);
        txt_image64 = (TextView) findViewById(R.id.txt_image64);
        image_select = (ImageView) findViewById(R.id.image_select);

        //Set de los controladores
        ic = new ImageController(1, Picker.PickMode.SINGLE_IMAGE);
        ic.addView(image_select);

        //Configuracion de Firebase
        Firebase.setAndroidContext(this);
        firebase = new Firebase(getString(R.string.firebase_url));

        if(firebase.getAuth() != null) {
            //Obtenemos el usuario que intenta registrarse
            newUser = (User) getIntent().getSerializableExtra("user");

            //Hay que llenar la informacion de registro con los datos del usuario
            switch (firebase.getAuth().getProvider()) {
                case "google" :
                    etxt_email.setText(newUser.getEmail());
                    etxt_email.setEnabled(false);
                    etxt_name.setText(newUser.getName());
                    etxt_name.setEnabled(false);
                    txt_image64.setText(newUser.getImage64());
                    txt_image64.setVisibility(View.GONE);
                    Glide.with(this).load(newUser.getImage64()).asBitmap().into(image_select);
                    break;
                case "facebook":
                    etxt_name.setText(newUser.getName());
                    etxt_name.setEnabled(false);
                    txt_image64.setText(newUser.getImage64());
                    txt_image64.setVisibility(View.GONE);
                    Glide.with(this).load(newUser.getImage64()).asBitmap().into(image_select);
                    break;
                case "password":
                    etxt_email.setText(newUser.getEmail());
                    etxt_email.setEnabled(false);
                    break;
                default:
                    Toast.makeText(this, "No ha ingresado con ninguna cuenta.", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            //Es un registro con correo y contraseña
            etxt_password.setVisibility(View.VISIBLE);
        }

    }



    public void attempt_register (View view) {
        if (validate_fields()) {
            final String email = etxt_email.getText().toString();
            final int age = Integer.parseInt(etxt_age.getText().toString());
            final String name = etxt_name.getText().toString();
            final String image64 = txt_image64.getText().toString();
            final String phone = etxt_phone.getText().toString();
            if(firebase.getAuth() != null) {
                //Salvar la informacion de registro de usuarios y verificar los campos faltantes.
                switch (firebase.getAuth().getProvider()) {
                    case "google":
                        newUser.setAge(age);
                        if(!phone.isEmpty()) {
                            newUser.setPhone(phone);
                        }
                        break;
                    case "facebook":
                        newUser.setEmail(email);
                        newUser.setAge(age);
                        if(!phone.isEmpty()) {
                            newUser.setPhone(phone);
                        }
                        break;
                    case "password":
                        newUser = new User();
                        newUser.setEmail(email);
                        newUser.setName(name);
                        newUser.setAge(age);
                        if(!image64.equals(R.string.label_image64)) {
                            newUser.setImage64(image64);
                        }
                        if(!phone.isEmpty()) {
                            newUser.setPhone(phone);
                        }
                        break;
                    default:
                        Toast.makeText(this, getString(R.string.not_account), Toast.LENGTH_SHORT);
                        break;
                }
                Firebase fbUser = firebase.child("users").child(newUser.getUid());
                fbUser.setValue(newUser);
                clear();
                Toast.makeText(RegisterActivity.this, getString(R.string.register_success), Toast.LENGTH_SHORT).show();
                to_App();
            } else {
                String password = etxt_password.getText().toString();
                if(!password.isEmpty()) {
                    firebase.createUser(email, password, this);

                } else {
                    Toast.makeText(this,getString(R.string.must_have_pass), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            showMessageDialog("Error",getString(R.string.empty_fields));
        }
    }

    public void select_image(View v) {
        ic.setMyContext(v.getContext());
        ic.selectImage();
    }


    private void showMessageDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public boolean validate_fields (){
        boolean correct = true;
        if( etxt_email.getText().toString().length() == 0 ) {
            etxt_email.setError(""+R.string.validate_email);
            correct = false;
        }
        if( etxt_name.getText().toString().length() == 0 ) {
            etxt_name.setError(""+R.string.validate_name);
            correct = false;
        }
        if( etxt_age.getText().toString().length() == 0 ) {
            etxt_age.setError(""+R.string.validate_age);
            correct = false;
        }
        return  correct;
    }

    public void clear() {
        etxt_email.setText("");
        etxt_name.setText("");
        etxt_age.setText("");
        txt_image64.setText("");
        etxt_phone.setText("");
    }

    private void to_App() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onSuccess(Map<String, Object> result) {
        String email = etxt_email.getText().toString();
        int age = Integer.parseInt(etxt_age.getText().toString());
        String name = etxt_name.getText().toString();
        String image64 = ic.toBase64();
        String phone = etxt_phone.getText().toString();
        User register = new User();
        register.setUid(result.get("uid").toString());
        register.setEmail(email);
        register.setName(name);
        register.setAge(age);
        if(!image64.isEmpty()) {
            register.setImage64(image64);
        }
        if(!phone.isEmpty()) {
            register.setPhone(phone);
        }
        Firebase fbUser = firebase.child("users").child(register.getUid());
        fbUser.setValue(register);
        clear();
        mPreferences.getInstance().Initialize(this);
        mPreferences.getInstance().setUserHasLocation(false);
        Toast.makeText(RegisterActivity.this, getString(R.string.register_successfully), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(FirebaseError firebaseError) {
        showMessageDialog("Error", firebaseError.getMessage());
    }
}
