package com.tracespot.view.Activity.view.activity;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.firebase.ui.auth.core.FirebaseAuthProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.MyLocationProvider;
import com.tracespot.view.Activity.model.SearchCategory;
import com.tracespot.view.Activity.model.UserLocation;
import com.tracespot.view.Activity.model.mPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by  tracespot on 03/05/16.
 */
public class NewPlaceActivity extends AppCompatActivity implements MyLocationProvider.LocationCallback, GoogleMap.OnCameraChangeListener, GeoQueryEventListener {

    private static final String TAG = "places_fragment";


    private GeoLocation center = new GeoLocation(10.085814, -84.471954);
    private static final int INITIAL_ZOOM_LEVEL = 14;
    private GoogleMap map;
    private MapView mapView;
    private Circle mSearchCircle;
    private MyLocationProvider locationProvider;
    private GeoQuery geoQuery;
    private Firebase firebase;
    private GeoFire geoFire;
    private MarkerOptions marker;
    private LatLng myLatlng;

    private CheckBox checkbox_pet;
    private CheckBox checkbox_person;
    private CheckBox checkbox_object;

    private EditText edt_name;
    private Button saveLocation;
    private String uid;
    private SearchCategory searchCategory;

    @Override
    public void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_place);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        firebase.setAndroidContext(this);

        locationProvider = new MyLocationProvider(this, this);
        firebase = new Firebase(getString(R.string.firebase_url));
//        firebase = new Firebase("https://dev-ts.firebaseio.com/locations");
        geoFire = new GeoFire(firebase);
        geoQuery = geoFire.queryAtLocation(center, 0.5);

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            Log.e(TAG, "unable to initialize map");
        }
        map = mapView.getMap();
        LatLng latlngCenter = new LatLng(center.latitude, center.longitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngCenter, INITIAL_ZOOM_LEVEL));

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                setLocation(marker);
            }
        });

        checkbox_pet = (CheckBox) findViewById(R.id.checkbox_mascot);
        checkbox_person = (CheckBox) findViewById(R.id.checkbox_person);
        checkbox_object = (CheckBox) findViewById(R.id.checkbox_object);

        uid = firebase.getAuth().getUid();

        edt_name = (EditText) findViewById(R.id.edt_name_location);
        saveLocation = (Button) findViewById(R.id.save_location);
        saveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserLocation userLocation = new UserLocation();
                final String nameLocation = edt_name.getText().toString();

                if (myLatlng == null) {
                    new MaterialDialog.Builder(NewPlaceActivity.this)
                            .content(getString(R.string.not_location_yet))
                            .positiveText(getString(R.string.gotit))
                            .show();
                } else {
                    if (!nameLocation.equals("")) {
                        final Firebase locationsKey = firebase.child("locations").push();
                        String key = locationsKey.getKey();
//                        firebase.child("locations").child(key).removeValue();
                        Firebase locations = firebase.child("locations");
                        geoFire  = new GeoFire(locations);

                        geoFire.setLocation(key, new GeoLocation(myLatlng.latitude, myLatlng.longitude));

                        locations.child(key).child("nameLocation").setValue(nameLocation);
                        locations.child(key).child("active").setValue(true);



                        Firebase mapRef = firebase.child("locationCategories").child(key);

                        Map<String, Object> mapLocations = new HashMap<String, Object>();

                                    if (checkbox_pet.isChecked()) {
                                        mapLocations.put("1", true);
                                    }
                                    if (checkbox_person.isChecked()) {
                                        mapLocations.put("2", true);
                                    }
                                    if (checkbox_object.isChecked()) {
                                        mapLocations.put("3", true);
                                    }

                                    if (mapLocations.size() == 0) {
                                        new MaterialDialog.Builder(NewPlaceActivity.this)
                                                .content(R.string.content)
                                                .positiveText(getString(R.string.gotit))
                                                .show();
                                    } else {
                                        mapRef.updateChildren(mapLocations);

                                        firebase.child("userLocations").child(uid).child(key).setValue(true);

                                        edt_name.setText("");
                                        checkbox_person.setChecked(false);
                                        checkbox_pet.setChecked(false);
                                        checkbox_object.setChecked(false);
                                        Toast.makeText(NewPlaceActivity.this, getString(R.string.location_saved), Toast.LENGTH_SHORT).show();
                                        mPreferences.getInstance().setUserHasLocation(true);
                                        finish();
                                    }
                    } else {
                        new MaterialDialog.Builder(NewPlaceActivity.this)
                                .content(getString(R.string.location_must_have_name))
                                .positiveText(getString(R.string.gotit))
                                .show();
                    }
            }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        locationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        locationProvider.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        geoQuery.removeAllListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        geoQuery.addGeoQueryEventListener(this);
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {

    }

    @Override
    public void onKeyExited(String key) {

    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(FirebaseError error) {

    }

    @Override
    public void handleNewLocation(Location location) {
        center = new GeoLocation(location.getLatitude(), location.getLongitude());
        LatLng latlng = new LatLng(center.latitude, center.longitude);


        marker = new MarkerOptions().position(new LatLng(center.latitude, center.longitude))
                .title(getString(R.string.title_marker))
                .draggable(true);

        myLatlng = marker.getPosition();
        Log.i(TAG, "latidud " + myLatlng.latitude + " longitud " + myLatlng.longitude);

        map.addMarker(marker);
        map.moveCamera(CameraUpdateFactory.newLatLng(latlng));
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        LatLng newCenter = cameraPosition.target;
        geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
    }

    public void setLocation(Marker marker) {
        myLatlng = marker.getPosition();
    }
}
