package com.tracespot.view.Activity.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tracespot on 28/04/2016.
 */
public class UserLocation implements Serializable{

    private String id;
    private String nameLocation;
    private boolean active;
    private double longitude;
    private double latitude;
    private ArrayList<SearchCategory> search_objects_list;

    public UserLocation(){
        this.search_objects_list = new ArrayList<>();
    }

    public UserLocation(String id, String nameLocation, double longitude, double latitude, Boolean active){
        this.id  = id;
        this.nameLocation = nameLocation;
        this.longitude = longitude;
        this.latitude = latitude;
        this.active = active;
        this.search_objects_list = new ArrayList<>();
    }

    public void addSearchCategory(SearchCategory searchCategory){
        this.search_objects_list.add(searchCategory);
    }

    public String getNameLocation() {
        return nameLocation;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setActive(Boolean active){
        this.active = active;
    }

    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {this.latitude = latitude;}

    public void setCategories(ArrayList<SearchCategory> lista){
        this.search_objects_list = lista;
    }

    public void setSearch_objects_list(ArrayList<SearchCategory> search_objects_list) {
        this.search_objects_list = search_objects_list;
    }



    public String getId(){
        return this.id;
    }

    public Boolean getActive(){
        return this.active;
    }

    public ArrayList<SearchCategory> getCategories(){
        return this.search_objects_list;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }


    public ArrayList<SearchCategory> getSearch_objects_list() {
        return search_objects_list;
    }

}
