package com.tracespot.view.Activity.model;

/**
 * Created by tracespot on 26/05/16.
 */
public class SpinnerItem {

    public String spinnerText;

    /**
     * variable to store the image of the spinhner object
     */
    public int spinnerImageId;

    public SpinnerItem() {

    }

    public SpinnerItem(String spinnerText, int spinnerImageId) {
        this.spinnerText = spinnerText;
        this.spinnerImageId = spinnerImageId;
    }
}
