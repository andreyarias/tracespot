package com.tracespot.view.Activity.view.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.User;

import net.yazeed44.imagepicker.util.Picker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tracespot on 25/04/16.
 */
public class ProfileFragment extends BaseFragment{
    private TextView txt_name;
    private TextView txt_email;
    private TextView txt_age;
    private TextView txt_phone;
    private ImageView image_image64;
    private FloatingActionButton fab_logout;
    private FloatingActionButton fab_change_password;
    private FloatingActionButton fab_change_info;
    private Firebase firebase;

    private EditText etxt_name;
    private EditText etxt_age;
    private EditText etxt_phone;

    private Firebase userInfo;

    private ImageController ic;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView =  super.onCreateView(inflater, container, savedInstanceState);

        txt_name = (TextView) rootView.findViewById(R.id.txt_name);
        txt_email = (TextView) rootView.findViewById(R.id.txt_email);
        txt_age = (TextView) rootView.findViewById(R.id.txt_age);
        txt_phone = (TextView) rootView.findViewById(R.id.txt_phone);
        image_image64 = (ImageView) rootView.findViewById(R.id.image_profile);
        image_image64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_image(v);
            }
        });

        fab_logout = (FloatingActionButton) rootView.findViewById(R.id.fab_logout);

        fab_change_info = (FloatingActionButton) rootView.findViewById(R.id.fab_change_info);
        fab_change_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change_information();
            }
        });

        //Set de los controladores
        ic = new ImageController(1, Picker.PickMode.SINGLE_IMAGE);
        ic.addView(image_image64);

        fab_change_password = (FloatingActionButton) rootView.findViewById(R.id.fab_change_password);
        fab_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type_password();

            }
        });

        Firebase.setAndroidContext(getActivity().getApplicationContext());

        firebase= new Firebase(getString(R.string.firebase_url));

        showInformation();
        return rootView;
    }

    private void showInformation() {
        userInfo = firebase.child("users").child(firebase.getAuth().getUid());
        userInfo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                String age = ("" + user.getAge());
                String phone = user.getPhone();
                String name = user.getName();
                String email = user.getEmail();
                String image64 = user.getImage64();
                txt_phone.setText(phone);
                txt_name.setText(name);
                txt_email.setText(email);

                if(user.getAge() == 0) {
                    txt_age.setText("age not set");
                } else  {
                    txt_age.setText(age);
                }
                if (getActivity() != null) {
                    ic.setMyContext(getActivity().getApplicationContext());
                    if (!image64.equals("image not set")) {
                        if (firebase.getAuth().getProvider().equals("password")){
                            ic.setImageToView(ic.toByteArray(image64), image_image64);
                        } else {
                            ic.setImageToView(image64, image_image64);
                            fab_change_password.setVisibility(View.GONE);
                        }
                    } else {
                        String url = firebase.getAuth().getProviderData().get("profileImageURL").toString();
                        ic.setImageToView(url, image_image64);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
    }

    public void select_image(View v) {
        ic.setMyContext(v.getContext());
        ic.selectImage();
    }

    @Override
    public void onResume() {
        super.onResume();
        saveImage();
    }

    private void saveImage() {
        ArrayList<String> paths = ic.getAsPaths();
        if (paths != null) {
            String encodedImage = ic.toBase64(paths.get(0));
            Firebase fbUser = firebase.child("users").child((firebase.getAuth().getUid()));
            Map<String, Object> image64 = new HashMap<String, Object>();
            image64.put("image64", encodedImage);
            fbUser.updateChildren(image64);
        }
    }

    @Override
    protected int getLayoutFromResource() {
        return R.layout.fragment_profile;
    }

    private void change_information () {
        fab_change_info.setVisibility(View.GONE);
        CardView form = (CardView) getActivity().findViewById(R.id.form_info);
        form.setVisibility(View.VISIBLE);

        Button button_save = (Button) getActivity().findViewById(R.id.button_save_changes);
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save_information();
            }
        });
    }
    private void save_information () {
        Firebase fbUser = firebase.child("users").child((firebase.getAuth().getUid()));
        etxt_age = (EditText) getActivity().findViewById(R.id.etxt_age);
        etxt_phone = (EditText) getActivity().findViewById(R.id.etxt_phone);
        etxt_name = (EditText) getActivity().findViewById(R.id.etxt_name);

        String age = etxt_age.getText().toString();
        String name = etxt_name.getText().toString();
        String phone = etxt_phone.getText().toString();
        if(age.length() > 0 && !age.matches("")) {
            fbUser.child("age").setValue(age);
        }
        if(name.length() > 0 && !name.matches("")) {
            fbUser.child("name").setValue(name);
        }
        if(phone.length() > 0 && !phone.matches("")) {
            fbUser.child("phone").setValue(phone);
        }
        clear();
        CardView form = (CardView) getActivity().findViewById(R.id.form_info);
        form.setVisibility(View.GONE);
        fab_change_info.setVisibility(View.VISIBLE);
    }


    private String newPassword;
    private String oldPassword;

    private void change_password () {
        firebase.changePassword(firebase.getAuth().getProviderData().get("email").toString(), oldPassword, newPassword, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                showMessageDialog(getString(R.string.pass_changed), getString(R.string.pass_changed_succesfully));
            }
            @Override
            public void onError(FirebaseError firebaseError) {
                // error encountered
                showMessageDialog(getString(R.string.pass_not_changed), getString(R.string.error_changing_pass));
            }
        });
        newPassword = "";
        oldPassword = "";
    }

    private void type_password () {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.actual_pass));
        // Set up the input
        final EditText input = new EditText(getContext());
        input.setHint(R.string.old_password);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                oldPassword = input.getText().toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.new_pass));
                // Set up the input
                final EditText input = new EditText(getContext());
                input.setHint(R.string.new_password);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                builder.setView(input);
                // Set up the buttons
                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newPassword = input.getText().toString();
                        change_password();
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void clear() {
        etxt_name.setText("");
        etxt_age.setText("");
        etxt_phone.setText("");
    }

    private void showMessageDialog(String title, String message) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}