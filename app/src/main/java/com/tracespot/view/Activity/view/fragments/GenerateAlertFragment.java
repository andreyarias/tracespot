package com.tracespot.view.Activity.view.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.MyLocationProvider;
import com.firebase.client.Firebase;
import com.tracespot.view.Activity.view.activity.GenerateAlertActivity;

import java.io.FileReader;

/**
 * Created by tracespot  on 25/04/16.
 */
public class GenerateAlertFragment extends BaseFragment implements MyLocationProvider.LocationCallback, GoogleMap.OnCameraChangeListener, GeoQueryEventListener {

    private static final String TAG = "generate_alert_fragment";


    private GeoLocation center = new GeoLocation(10.085814, -84.471954);
    private static final int INITIAL_ZOOM_LEVEL = 14;
    private GoogleMap map;
    private MapView mapView;
    private Button buttonGenerate;
    private MyLocationProvider locationProvider;
    private GeoQuery geoQuery;
    private Firebase firebase;
    private GeoFire geoFire;
    private MarkerOptions marker;
    private LatLng myLatlng;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebase.setAndroidContext(getActivity());
        locationProvider = new MyLocationProvider(getActivity(), this);
        firebase = new Firebase(getString(R.string.firebase_url));
        geoFire = new GeoFire(firebase);
        geoQuery = geoFire.queryAtLocation(center, 0.5);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        mapView = (MapView) rootView.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            Log.e(TAG, "unable to initialize map");
        }
        map = mapView.getMap();
        buttonGenerate = (Button) rootView.findViewById(R.id.btn_generate_alert);
        LatLng latlngCenter = new LatLng(center.latitude, center.longitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngCenter, INITIAL_ZOOM_LEVEL));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                setLocation(marker);
            }
        });
        buttonGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                myLatlng = marker.getPosition();
                Intent intent = new Intent(getActivity(), GenerateAlertActivity.class);
                intent.putExtra("latitude", myLatlng.latitude);
                intent.putExtra("longitude", myLatlng.longitude);
                getActivity().startActivity(intent);
                map.clear();

            }
        });

        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // Getting Current Location
        if(manager != null){
            if(!manager.isProviderEnabled( LocationManager.GPS_PROVIDER )){
                showMessageNoGPS();
            }
        }

    }

    private void showMessageNoGPS() {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.gps))
                .content(getString(R.string.must_have_activate_gps))
                .positiveText(getString(R.string.activate_gps))
                .autoDismiss(true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        PackageManager packageManager = getActivity().getPackageManager();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        if (intent.resolveActivity(packageManager) != null) {
                            startActivity(intent);
                        }   else  {
                            Toast.makeText(getActivity(), getString(R.string.cant_activate_gps), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "No Intent available to handle action");
                        }
                    }
                })
                .show();
    }

    @Override
    public void handleNewLocation(Location location) {
        center = new GeoLocation(location.getLatitude(), location.getLongitude());
        LatLng latlng = new LatLng(center.latitude, center.longitude);

        marker = new MarkerOptions().position(new LatLng(center.latitude, center.longitude))
                .title(getString(R.string.title_marker)).draggable(true);
        myLatlng = marker.getPosition();

        map.addMarker(marker);
//        map.addMarker(new MarkerOptions().position(new LatLng(center.latitude, center.longitude))
//                .title("current location").draggable(true));
//        MarkerOptions options = new MarkerOptions()
//                .position(latlng)
//                .title("Tu ubicación!");
//        map.addMarker(options);
        map.moveCamera(CameraUpdateFactory.newLatLng(latlng));

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        locationProvider.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        locationProvider.disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        geoQuery.removeAllListeners();
    }

    @Override
    public void onStart() {
        super.onStart();
        geoQuery.addGeoQueryEventListener(this);
    }

    @Override
    protected int getLayoutFromResource() {
        return R.layout.fragment_generate_alert;
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        LatLng newCenter = cameraPosition.target;
        geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {

    }

    @Override
    public void onKeyExited(String key) {

    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(FirebaseError error) {

    }

    public void setLocation(Marker marker) {
        myLatlng = marker.getPosition();
    }
}