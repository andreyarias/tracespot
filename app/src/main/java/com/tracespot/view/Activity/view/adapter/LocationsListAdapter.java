package com.tracespot.view.Activity.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.tracespot.R;
import com.tracespot.view.Activity.model.Alert;
import com.tracespot.view.Activity.model.SearchCategory;
import com.tracespot.view.Activity.model.UserLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tracespot on 04/06/2016.
 */
public class LocationsListAdapter extends BaseAdapter  {

    private Query mRef;
    private Class<UserLocation> mModelClass;
    private int mLayout;
    private LayoutInflater mInflater;
    private List<UserLocation> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    private Context context;

    public List<UserLocation> getModels(){
        return this.mModels;
    }

    public void setModels(List<UserLocation> models){
        this.mModels = models;
    }

    /**
     * @param mRef        The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                    combination of <code>limit()</code>, <code>startAt()</code>, and <code>endAt()</code>,
     * @param mModelClass Firebase will marshall the data at a location into an instance of a class that you provide
     * @param mLayout     This is the mLayout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of mModelClass.
     * @param context    The activity containing the ListView
     */
    public LocationsListAdapter(Query mRef, Class<UserLocation> mModelClass, int mLayout, Context context) {
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        this.mLayout = mLayout;
        this.context = context;
        mModels = new ArrayList<UserLocation>();
        mKeys = new ArrayList<String>();

        mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                if(dataSnapshot != null) {
                    final UserLocation model = new UserLocation();
                    model.setId(dataSnapshot.getKey());

                    Firebase firebase_referencee = new Firebase("https://dev-ts.firebaseio.com/");
                    firebase_referencee.child("locations").child(dataSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot != null) {
                                if(dataSnapshot.hasChildren()) {
                                    model.setActive(dataSnapshot.child("active").getValue(Boolean.class));
                                    for (DataSnapshot snap : dataSnapshot.child("l").getChildren()) {
                                        if (snap.getKey() == "0") {
                                            model.setLatitude(snap.getValue(Double.class));
                                        }
                                        if (snap.getKey() == "1") {
                                            model.setLongitude(snap.getValue(Double.class));
                                        }
                                    }
                                    model.setNameLocation(dataSnapshot.child("nameLocation").getValue(String.class));
                                    Firebase firebase_referenceee = new Firebase("https://dev-ts.firebaseio.com");
                                    firebase_referenceee.child("locationCategories").child(model.getId()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            model.getSearch_objects_list().clear();
                                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                                model.getSearch_objects_list().add(new SearchCategory(Integer.parseInt(snap.getKey())));
                                            }

                                            notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onCancelled(FirebaseError firebaseError) {

                                        }
                                    });
                                    notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });


                    String key = model.getId();

                    // Insert into the correct location, based on previousChildName
                    if (previousChildName == null) {
                        mModels.add(0, model);
                        mKeys.add(0, key);
                    } else {
                        int previousIndex = mKeys.indexOf(previousChildName);
                        int nextIndex = previousIndex + 1;
                        if (nextIndex == mModels.size()) {
                            mModels.add(model);
                            mKeys.add(key);
                        } else {
                            mModels.add(nextIndex, model);
                            mKeys.add(nextIndex, key);
                        }
                    }

                    notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                // A model was removed from the list. Remove it from our list and the name mapping
                String key = dataSnapshot.getKey();
                int index = mKeys.indexOf(key);

                mKeys.remove(index);
                mModels.remove(index);

                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }

        });
    }

    public void cleanup() {
        // We're being destroyed, let go of our mListener and forget about all of the mModels
        mRef.removeEventListener(mListener);
        mModels.clear();
        mKeys.clear();
    }

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int i) {
        return mModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(mLayout, viewGroup, false);
        }

        UserLocation model = mModels.get(i);
        // Call out to subclass to marshall this model into the provided view
        populateView(view, model);
        return view;
    }

    /**
     * Each time the data at the given Firebase location changes, this method will be called for each item that needs
     * to be displayed. The arguments correspond to the mLayout and mModelClass given to the constructor of this class.
     * <p/>
     * Your implementation should populate the view using the data contained in the model.
     *
     * @param itemView     The view to populate
     * @param model The object containing the data used to populate the view
     */
    public  void populateView(View itemView, UserLocation model){
        if(model != null) {


            TextView location_name = (TextView) itemView.findViewById(R.id.text_location_name);
            TextView location_mascot_icon = (TextView) itemView.findViewById(R.id.text_location_icon_pet);
            TextView location_person_icon = (TextView) itemView.findViewById(R.id.text_location_icon_person);
            TextView location_object_icon = (TextView) itemView.findViewById(R.id.text_location_icon_object);

            location_name.setText(model.getNameLocation());

            if (model.getActive()) {
                Drawable img = this.context.getResources().getDrawable(R.drawable.ic_done_black_24dp);
                location_name.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            } else {
                Drawable img = this.context.getResources().getDrawable(R.drawable.ic_clear_black_24dp);
                location_name.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }

            location_mascot_icon.setVisibility(location_mascot_icon.INVISIBLE);
            location_person_icon.setVisibility(location_person_icon.INVISIBLE);
            location_object_icon.setVisibility(location_object_icon.INVISIBLE);

            for (SearchCategory category : model.getCategories()) {
                if (category.getId() == 1) {
                    location_mascot_icon.setVisibility(location_mascot_icon.VISIBLE);
                }
                if (category.getId() == 2) {
                    location_person_icon.setVisibility(location_person_icon.VISIBLE);
                }
                if (category.getId() == 3) {
                    location_object_icon.setVisibility(location_object_icon.VISIBLE);
                }
            }
        }
    }



}
