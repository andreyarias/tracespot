package com.tracespot.view.Activity.controller;

import android.app.DownloadManager;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.tracespot.R;

/**
 * Created by tracespot  on 11/06/16.
 */
public class AlertsReceivedController implements GeoQueryEventListener{


    private Firebase firebase;
    private Firebase locationsRef;
    private GeoFire geoFire;
    private GeoLocation center;
    private GeoQuery geoQuery;
    private double latitude;
    private double longitude;
    private double radius;
    private int idCategory;
    private String alertKey;
    private String uid;

    public AlertsReceivedController(double latitude, double longitude, double radius, int idCategory, String alertKey, String uid) {
        firebase = new Firebase("https://dev-ts.firebaseio.com");
        locationsRef = new Firebase("https://dev-ts.firebaseio.com/locations");
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.idCategory = idCategory;
        this.alertKey = alertKey;
        this.uid = uid;
        createObjects();
    }

    public AlertsReceivedController() {
        firebase = new Firebase("https://dev-ts.firebaseio.com");
        locationsRef = new Firebase("https://dev-ts.firebaseio.com/locations");
    }

    public void getData(double latitude, double longitude, double radius, int idCategory, String alertKey, String uid) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.idCategory = idCategory;
        this.alertKey = alertKey;
        this.uid = uid;
        createObjects();
    }

    public void createObjects() {

        geoFire = new GeoFire(locationsRef);
        center = new GeoLocation(this.latitude, this.longitude);

        geoQuery = this.geoFire.queryAtLocation(center, this.radius);
        geoQuery.addGeoQueryEventListener(this);
    }


    @Override
    public void onKeyEntered(final String key, GeoLocation location) {

        firebase.child("locations").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("active").getValue(Boolean.class)) {
                    firebase.child("userLocations").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snap: dataSnapshot.getChildren()) {
                                if (!snap.getKey().equals(uid)) {
                                    for (DataSnapshot snapshot: snap.getChildren()) {
                                        if (key.equals(snapshot.getKey())) {
                                            firebase.child("locationCategories").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                                                        if (idCategory == Integer.parseInt(snapshot.getKey())) {
                                                            if (alertKey != null) {
                                                                firebase.child("alertsReceived").child(key).child(alertKey).setValue(true);
                                                            }
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(FirebaseError firebaseError) {

                                                }
                                            });
                                        }
                                    }
                                }
                            }


                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    public void onKeyExited(String key) {

    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(FirebaseError error) {

    }


    public void onStop() {
        geoQuery.removeAllListeners();
    }

    public void onStart() {
        geoQuery.addGeoQueryEventListener(this);
    }
}
