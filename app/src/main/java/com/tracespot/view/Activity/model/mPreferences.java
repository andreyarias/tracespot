package com.tracespot.view.Activity.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

/**
 * Created by tracespot on 6/19/2016.
 */
public class mPreferences {
    private static mPreferences mInstance;
    private Context mContext;
    //
    private SharedPreferences mSharedPreferences;

    private mPreferences(){ }

    public static mPreferences getInstance(){
        if (mInstance == null) mInstance = new mPreferences();
        return mInstance;
    }

    public void Initialize(Context ctxt){
        mContext = ctxt;
        //
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void writePreference(String key, Object value){
        SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        prefsEditor.putString(key, json);
        prefsEditor.commit();
    }

    public Alert readAlertStored(String key, int mode) {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(key, "");
        switch (mode) {
            case 0:
                Alert sent = gson.fromJson(json, Alert.class);
                return sent;
            case 1:
                AlertReceived received = gson.fromJson(json, AlertReceived.class);
                return received;
            default:
                break;
        }
        return null;
    }

    public UserLocation readLocation(String key) {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(key, "");
        UserLocation mLocation = gson.fromJson(json, UserLocation.class);
        return mLocation;
    }

    public void setUserHasLocation(Boolean state) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("hasLocation", state);
        editor.apply();
    }

    public boolean getUserHasLocation() {
        boolean haslocation = mSharedPreferences.getBoolean("hasLocation", false);
        return haslocation;
    }

}
