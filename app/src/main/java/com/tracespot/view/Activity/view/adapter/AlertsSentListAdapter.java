package com.tracespot.view.Activity.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.Alert;
import com.tracespot.view.Activity.model.AlertReceived;
import com.tracespot.view.Activity.model.SearchCategory;
import com.tracespot.view.Activity.model.UserLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tracespot on 09/06/2016.
 */
public class AlertsSentListAdapter extends BaseAdapter {

    private Firebase firebase_reference;
    private String uid;
    private int mLayout;
    private List<Alert> mModels;
    private List<String> mKeys;
    private Context context;


    public List<Alert> getAlerts(){
        return this.mModels;
    }

    public void setAlerts(List<Alert> models){
        this.mModels = models;
    }

    /**
     * @param mLayout     This is the mLayout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of mModelClass.
     * @param context    The activity containing the ListView
     */
    public AlertsSentListAdapter(int mLayout, Context context) {
        Firebase.setAndroidContext(context);
        this.firebase_reference = new Firebase("https://dev-ts.firebaseio.com/");
        uid = firebase_reference.getAuth().getUid();
        this.mLayout = mLayout;
        this.context = context;
        mModels = new ArrayList<Alert>();
        mKeys = new ArrayList<String>();
        // Look for all child events. We will then map them to our own internal ArrayList, which backs ListView
        firebase_reference.child("alertsSent").child(uid).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                if(dataSnapshot != null) {

                        final String key = dataSnapshot.getKey();
                        final String previous = previousChildName;

                        final Alert alert = new Alert();
                        alert.setIdAlert(dataSnapshot.getKey());


                        firebase_reference.child("alerts").child(alert.getIdAlert()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    if(dataSnapshot.hasChildren()) {
                                        if (dataSnapshot.child("state").getValue(Integer.class) == 0) {

                                            alert.setCategory(dataSnapshot.child("category").getValue(Integer.class));
                                            alert.setDescriptionAlert(dataSnapshot.child("descriptionAlert").getValue(String.class));
                                            alert.setLatitude(dataSnapshot.child("latitude").getValue(Double.class));
                                            alert.setLongitude(dataSnapshot.child("longitude").getValue(Double.class));
                                            alert.setOwner(dataSnapshot.child("owner").getValue(String.class));
                                            alert.setRange(dataSnapshot.child("range").getValue(Integer.class));
                                            alert.setReported(dataSnapshot.child("reported").getValue(Integer.class));
                                            alert.setRewardAlert(dataSnapshot.child("rewardAlert").getValue(String.class));
                                            alert.setState(dataSnapshot.child("state").getValue(Integer.class));
                                            alert.setTitleAlert(dataSnapshot.child("titleAlert").getValue(String.class));


                                            firebase_reference.child("images").child(alert.getIdAlert()).addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                                        alert.getImages().add(snap.getValue(String.class));
                                                    }
                                                    notifyDataSetChanged();
                                                }

                                                @Override
                                                public void onCancelled(FirebaseError firebaseError) {

                                                }
                                            });
                                            if (!mModels.contains(alert)) {
                                                if (previous == null) {
                                                    mModels.add(0, alert);
                                                    mKeys.add(0, key);
                                                } else {
                                                    int previousIndex = mKeys.indexOf(previous);
                                                    int nextIndex = previousIndex + 1;
                                                    if (nextIndex == mModels.size()) {
                                                        mModels.add(alert);
                                                        mKeys.add(key);
                                                    } else {
                                                        mModels.add(nextIndex, alert);
                                                        mKeys.add(nextIndex, key);
                                                    }
                                                }
                                                notifyDataSetChanged();
                                            }
                                        } else {
                                            if(mModels.contains(alert)){
                                                mModels.remove(alert);
                                                mKeys.remove(alert.getIdAlert());
                                                notifyDataSetChanged();
                                            }
                                        }
                                    }

                                }

                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });

                    }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){

                String key = dataSnapshot.getKey();
                int index = mKeys.indexOf(key);

                mKeys.remove(index);
                mModels.remove(index);

                notifyDataSetChanged();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public void cleanup() {
        // We're being destroyed, let go of our mListener and forget about all of the mModels

        mModels.clear();
        mKeys.clear();
    }

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int i) {
        return mModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(mLayout, viewGroup, false);
        }

        Alert model = mModels.get(i);
        // Call out to subclass to marshall this model into the provided view
        populateView(view, model);
        return view;
    }

    /**
     * Each time the data at the given Firebase location changes, this method will be called for each item that needs
     * to be displayed. The arguments correspond to the mLayout and mModelClass given to the constructor of this class.
     * <p/>
     * Your implementation should populate the view using the data contained in the model.
     *
     * @param itemView     The view to populate
     * @param model The object containing the data used to populate the view
     */
    public  void populateView(View itemView, Alert model){

        TextView alert_title = (TextView) itemView.findViewById(R.id.txt_alert_sent_name);
        TextView people_reached = (TextView) itemView.findViewById(R.id.txt_people_reached);
        ImageView alert_image = (ImageView) itemView.findViewById(R.id.image_alert_sent_thumbnail);

        if(model != null){
            ImageController imageController = new ImageController(this.context);

            alert_title.setText(model.getTitleAlert());
            people_reached.setText(model.getDescriptionAlert());
            if(model.getImages().size() > 0) {
                imageController.setImageToView(imageController.toByteArray(model.getImages().get(0)), alert_image);
            }
        }
    }


}