package com.tracespot.view.Activity.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tracespot on 26/05/16.
 */
public class Alert implements Serializable{

    private String idAlert;
    private String owner;
    private String titleAlert;
    private String descriptionAlert;
    private String rewardAlert;
    private Double latitude;
    private Double longitude;
    private List<String> images;
    private int range;
    private String createdAt;
    private String finishedAt;

    /**
     * Estado de una alerta: 0 si esta activa, 1 si esta suspendida y 2 si esta finalizada.
     */
    private int state;

    /**
     * Contador que indica la cantidad de veces que ha sido reportada.
     */
    private int reported;
    private int category;

    public Alert() {
        images = new ArrayList<>(3);
    }

    public Alert(String idAlert, String owner, String titleAlert, String descriptionAlert, String rewardAlert, Double latitude, Double longitude, int range, int state, int category, int reported) {
        this.idAlert = idAlert;
        this.owner = owner;
        this.titleAlert = titleAlert;
        this.descriptionAlert = descriptionAlert;
        this.rewardAlert = rewardAlert;
        this.latitude = latitude;
        this.longitude = longitude;
        this.images = new ArrayList<>(3);
        this.range = range;
        this.category = category;
        this.state = state;
        this.reported = reported;
    }


    public String getIdAlert() {
        return idAlert;
    }

    public void setIdAlert(String idAlert) {
        this.idAlert = idAlert;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitleAlert() {
        return titleAlert;
    }

    public void setTitleAlert(String titleAlert) {
        this.titleAlert = titleAlert;
    }

    public String getDescriptionAlert() {
        return descriptionAlert;
    }

    public void setDescriptionAlert(String descriptionAlert) {
        this.descriptionAlert = descriptionAlert;
    }

    public String getRewardAlert() {
        return rewardAlert;
    }

    public void setRewardAlert(String rewardAlert) {
        this.rewardAlert = rewardAlert;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getReported() {
        return reported;
    }

    public void setReported(int reported) {
        this.reported = reported;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }
}
