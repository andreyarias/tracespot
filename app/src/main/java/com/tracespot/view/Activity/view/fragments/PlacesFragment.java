package com.tracespot.view.Activity.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.Query;
import com.tracespot.R;
import com.tracespot.view.Activity.model.SearchCategory;
import com.tracespot.view.Activity.model.UserLocation;
import com.tracespot.view.Activity.view.activity.LocationDetailActivity;
import com.tracespot.view.Activity.view.activity.NewPlaceActivity;
import com.tracespot.view.Activity.view.adapter.LocationsListAdapter;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by  tracespot on 25/04/16.
 */
public class PlacesFragment extends BaseFragment {

    private ListView list_locations;
    private ArrayList<UserLocation> list_of_locations = new ArrayList<UserLocation>();
    private ArrayAdapter<UserLocation> location_list_adapter;
    private Button createNewPlace;
    private View rootView;
    private ArrayList<String> location_id_list;

    private LocationsListAdapter adapter;


    private Firebase firebase_reference;
    private String ui = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View rootView =  super.onCreateView(inflater, container, savedInstanceState);
        this.rootView = rootView;
        Firebase.setAndroidContext(getActivity());
        firebase_reference = new Firebase(getString(R.string.firebase_url));
        String ui = firebase_reference.getAuth().getUid();
        Context context = getActivity();
        Query query = firebase_reference.child("userLocations").child(ui);
        Class<UserLocation> modelo = UserLocation.class;
        int idLayout = R.layout.fragment_view_location_in_list;

        this.adapter = new LocationsListAdapter(query, modelo,idLayout,context);

        ListView list_view = (ListView) rootView.findViewById(R.id.list_locations);
        list_view.setAdapter(adapter);
        registerClickCallback(rootView);

        createNewPlace = (Button) rootView.findViewById(R.id.button_add_new_location);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createNewPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewPlaceActivity.class);
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    protected int getLayoutFromResource() {
        return R.layout.fragment_places;
    }


    private void registerClickCallback(View vista){
        ListView list = (ListView) vista.findViewById(R.id.list_locations);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked , int position, long id) {
                UserLocation location = adapter.getModels().get(position);
                Intent new_activity = new Intent(getActivity(), LocationDetailActivity.class);
                new_activity.putExtra("userLocation", location);
                startActivity(new_activity);
            }
        });
    }
}