package com.tracespot.view.Activity.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tracespot.R;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by tracespot on 5/31/2016.
 */
public class ImageController implements Picker.PickListener {


    private Context myContext;
    private ArrayList<ImageEntry> myImages;
    private int myLimit;
    private Picker.PickMode myMode;
    private ArrayList<ImageView> myImageViews;

    public ImageController(Context context) {
        this.myContext = context;
        myImageViews = new ArrayList<>();
        myImageViews = new ArrayList<>();
    }

    public ImageController(int imageLimit, Picker.PickMode mode) {
        this.myContext = null;
        this.myImages = new ArrayList<>(imageLimit);
        this.myImageViews = new ArrayList<>(imageLimit);
        this.myLimit = imageLimit;
        this.myMode = mode;
    }

    public ImageController(Context c, int imageLimit, Picker.PickMode mode) {
        this.myContext = c;
        this.myImages = new ArrayList<>(imageLimit);
        this.myImageViews = new ArrayList<>(imageLimit);
        this.myLimit = imageLimit;
        this.myMode = mode;
    }

    public void setMyContext(Context myContext) {
        this.myContext = myContext;
    }

    @Override
    public void onPickedSuccessfully(ArrayList<ImageEntry> images) {
        for (ImageEntry image :
                images) {
            myImages.add(image);
        }
        refreshViews();
    }

    @Override
    public void onCancel() {
        //User canceled the pick activity
        Toast.makeText(myContext, "Cancelaste el cambio de imagen", Toast.LENGTH_SHORT).show();
    }

    public void selectImage() {
        myImages.clear();
        new Picker.Builder(myContext,this, R.style.AppTheme_NoActionBar)
                .setPickMode(myMode)
                .setLimit(myLimit)
                .build().startActivity();
    }

    private void setImageToView(int image, ImageView view) {
        if(image < (myImages.size())) {
            Glide.with(myContext).load(myImages.get(image).path).asBitmap().into(view);
        }
    }

    private void setImageToText(int image, TextView text) {
        String base64 = toBase64(image);
        text.setText(base64);
    }

    //Metodo utilizado para transformar una archivo imagen en string Base 64.
    public String toBase64 (int image) {
        Bitmap bm = BitmapFactory.decodeFile(myImages.get(image).path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public String toBase64 (String imagePath) {
        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public String toBase64() {
        if(!myImages.isEmpty()) {
            Bitmap bm = BitmapFactory.decodeFile(myImages.get(0).path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        }
        return "";
    }

    public void setImageToView(byte[] imageArray, ImageView view ){
        Glide.with(myContext).load(imageArray).asBitmap().into(view);
    }

    public void setImageToView(String url, ImageView view) {
        Glide.with(myContext).load(url).asBitmap().into(view);
    }

    //Método utilizado para transformar un string base 64 a un Bitmap
    public byte[] toByteArray (String base64) {
        return Base64.decode(base64, Base64.DEFAULT);
    }

    public void refreshViews() {
        if (!myImageViews.isEmpty()) {
            for (int i = 0; i < myImageViews.size(); i++) {
                setImageToView(i, myImageViews.get(i));
            }
        }
    }

    public ArrayList<String> getAsPaths() {
        ArrayList<String> paths = new ArrayList<>();
        if (!myImages.isEmpty()) {
            for (ImageEntry image: myImages) {
                paths.add(image.path);
            }
            return paths;
        }
        return null;
    }

    public ArrayList<String> getAsBase64() {
        ArrayList<String> base64 = new ArrayList<>();
        if (!myImages.isEmpty()) {
            try {
                for (ImageEntry image: myImages) {
                    base64.add(toBase64(image.path));
                }
            } catch (OutOfMemoryError error) {
                Toast.makeText(myContext, "No hay suficiente espacio en memoria para guardar las imagenes.", Toast.LENGTH_SHORT).show();
                base64 = null;
            }
            return base64;
        }
        return null;
    }

    public void addView(ImageView view) {
        myImageViews.add(view);
    }

}
