package com.tracespot.view.Activity.controller;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

/**
 * Created by tracespot on 4/24/2016.
 */
public class UserController {
    private static UserController ourInstance = new UserController();

    public static UserController getInstance() {
        return ourInstance;
    }

    private UserController() {
        firebase = new Firebase("https://dev-ts.firebaseio.com/");
    }

    private Firebase firebase;
    private AuthData authenticatedUser;
    private Firebase.AuthStateListener sessionListener;

    public void attempt_login (String email, String password) {
        firebase.authWithPassword(email, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                setAuthenticatedUser(authData);
            }
            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {

            }
        });
    }


    public AuthData getAuthenticatedUser() {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(AuthData authData) {
        this.authenticatedUser = authData;
    }

    private void logout() {
        if (this.authenticatedUser != null) {
            /* logout of Firebase */
            firebase.unauth();
            /* Logout of any of the Frameworks. This step is optional, but ensures the user is not logged into
             * Facebook/Google+ after logging out of Firebase. */
//            if (this.authenticatedUser.getProvider().equals("facebook")) {
//                /* Logout from Facebook */
//                LoginManager.getInstance().logOut();
//            } else if (this.mAuthData.getProvider().equals("google")) {
//                /* Logout from Google+ */
//                if (mGoogleApiClient.isConnected()) {
//                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//                    mGoogleApiClient.disconnect();
//                }
//            }
            /* Update authenticated user and show login buttons */
            setAuthenticatedUser(null);
        }
    }
}
