package com.tracespot.view.Activity.model;

import java.io.Serializable;

/**
 * Created by tracespot on 30/04/2016.
 */
public class SearchCategory implements Serializable{

    private int id;
    private String name;
    private String icon_route;
    private int max_range;

    public SearchCategory() {
    }

    public SearchCategory(int id, String name, String icon_route, int max_range){
        this.id = id;
        this.name = name;
        this.icon_route = icon_route;
        this.max_range = max_range;
    }

    public SearchCategory(int id){
        this.id = id;
    }


    public String getName(){
        return this.name;
    }

    public String getRoute(){
        return this.icon_route;
    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon_route() {
        return icon_route;
    }

    public void setIcon_route(String icon_route) {
        this.icon_route = icon_route;
    }

    public int getMax_range() {
        return max_range;
    }

    public void setMax_range(int max_range) {
        this.max_range = max_range;
    }
}
