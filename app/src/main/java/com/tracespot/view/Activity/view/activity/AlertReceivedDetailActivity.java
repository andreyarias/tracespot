package com.tracespot.view.Activity.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.Firebase;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.AlertReceived;
import com.tracespot.view.Activity.model.User;
import com.tracespot.view.Activity.model.mPreferences;

import net.yazeed44.imagepicker.util.Picker;

import java.util.ArrayList;

public class AlertReceivedDetailActivity extends AppCompatActivity {

    private static final String TAG = "alertreceived";
    private static final int INITIAL_ZOOM_LEVEL = 14;
    public static final int LIMITE_DENUNCIAS = 3;

    //CardView alert_info
    private TextView titleAlert;
    private TextView descriptionAlert;
    private TextView rewardAlert;
    private TextView stateAlert;
    private TextView typeAlert;

    //CardView alert_images
    private ArrayList<ImageView> alertImages;

    //Cardview alert_user
    private ImageView image_user_picture;
    private TextView text_user_name;
    private TextView text_user_email;
    private TextView text_user_phone;
    private FloatingActionButton alertMap;
    private FloatingActionButton contactUser;


    //CardView alert_actions
    private FloatingActionButton shareAlert;
    private FloatingActionButton reportAlert;

    //Models and Controllers
    private ImageController ic;
    private User myAlertUser;
    private AlertReceived myAlert;

    //Firebase objects
    private Firebase firebase;

    //Map view
    private MapView mapView;
    private View viewMap;

    //Dialogo de imagen detallada
    private ImageView imageView;
    private View viewImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_received_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //inicializacion de los componentes
        initializeComponents();
        setAlert(savedInstanceState);

    }

    private void initializeComponents() {
        //Inicializacion de Firebase
        firebase = new Firebase(getString(R.string.firebase_url));

        //Inicializacion de componentes botones
        shareAlert = (FloatingActionButton) findViewById(R.id.button_share_alert);
        reportAlert = (FloatingActionButton) findViewById(R.id.button_report_alert);

        //Inicializacion de componentes de usuario
        text_user_email = (TextView) findViewById(R.id.text_user_email);
        text_user_name =  (TextView) findViewById(R.id.text_user_name);
        text_user_phone = (TextView) findViewById(R.id.text_user_phone);
        image_user_picture = (ImageView) findViewById(R.id.image_user_picture);

        //Inicializacion de componentes de alerta
        titleAlert = (TextView) findViewById(R.id.txt_alert_title);
        descriptionAlert = (TextView) findViewById(R.id.txt_alert_description);
        rewardAlert = (TextView) findViewById(R.id.txt_alert_reward);
        stateAlert = (TextView) findViewById(R.id.txt_alert_state);
        typeAlert = (TextView) findViewById(R.id.textview_alert_type);

        //Obtener los image views del layout y agregarlos a la lista
        alertImages = new ArrayList<>();
        alertImages.add((ImageView) findViewById(R.id.image_alertFront));
        alertImages.add((ImageView) findViewById(R.id.image_alertSide));
        alertImages.add((ImageView) findViewById(R.id.image_alertBack));


        //Inicializar el controlador de imagenes
        ic = new ImageController(this, 3, Picker.PickMode.MULTIPLE_IMAGES);

        //Agregar los views al controlador para refrescarlos dinamicamente
        for (ImageView iv:alertImages) {
            ic.addView(iv);
        }
    }

    /**
     * Crea la vista del mapa para luego asignarsela al dialogo del mapview
     * @param savedInstanceState
     */
    public void createViewMap(Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content);
        viewMap = LayoutInflater.from(this).inflate(R.layout.dialog_alert_detail_map, viewGroup, false);
        mapView = (MapView) viewMap.findViewById(R.id.map_alert_detail);
        mapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            Log.e(TAG, "unable to initialize map");
        }
        GoogleMap map = mapView.getMap();
        LatLng latlngCenter = new LatLng(myAlert.getLatitude(), myAlert.getLongitude());// myAlert.getLocation().getLatitude(), myAlert.getLocation().getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngCenter, INITIAL_ZOOM_LEVEL));
        map.addMarker(new MarkerOptions().position(latlngCenter)
                .title("current location"));

    }

    /**
     * Crea la vista del imagen para luego asignarsela al dialogo del mapview
     */
    public void createViewImage() {
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content);
        viewImage = LayoutInflater.from(this).inflate(R.layout.dialog_image_detail, viewGroup, false);
        imageView = (ImageView) viewImage.findViewById(R.id.imageView_image_detail);
    }

    private void setAlert(Bundle savedInstanceState) {
        //Obtener la alerta que se quiere detallar
        myAlert = (AlertReceived) mPreferences.getInstance().readAlertStored("receivedAlert", 1);
        if(myAlert != null) {
            myAlertUser = myAlert.getUser();
            showUser();
            showAlertInformation();
            createViewMap(savedInstanceState);
            createViewImage();
        } else {
            Toast.makeText(this, "La alerta no se ha terminado de cargar.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Muestra la informacion de una alerta, es decir, el detalle de la misma.
     */
    public void showAlertInformation() {
        titleAlert.setText(myAlert.getTitleAlert());
        descriptionAlert.setText(myAlert.getDescriptionAlert());
        rewardAlert.setText(myAlert.getRewardAlert());
        if (myAlert.getState() == 0) {
            stateAlert.setText(R.string.alert_state_active);
        } else {
            if (myAlert.getState() == 2)  {
                stateAlert.setText(R.string.alert_state_finished);
            } else {
                stateAlert.setText(R.string.alert_state_suspended);
            }
        }
        for (int i = 0; i < myAlert.getImages().size(); i++) {
            ic.setImageToView(ic.toByteArray(myAlert.getImages().get(i)), alertImages.get(i));
        }
        switch (myAlert.getCategory()) {
            case 1:
                typeAlert.setText(getString(R.string.checklist_pet));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_pets, 0, 0, 0);
                break;
            case 2:
                typeAlert.setText(getString(R.string.checklist_person));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_persons, 0, 0, 0);
                break;
            case 3:
                typeAlert.setText(getString(R.string.checklist_object));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_other_objects, 0, 0, 0);
                break;
            default:
                break;
        }
    }

    /**
     * Habilita los cambios de edicion.
     */
    public void enableFields() {
        titleAlert.setEnabled(true);
        descriptionAlert.setEnabled(true);
        rewardAlert.setEnabled(true);
        typeAlert.setEnabled(true);
    }

    /**
     * Deshabilita los campos de edicion.
     */
    public void disableFields() {
        titleAlert.setEnabled(false);
        descriptionAlert.setEnabled(false);
        rewardAlert.setEnabled(false);
        typeAlert.setEnabled(false);
    }

    /**
     * Muestra la informacion del usuario. Solo se ejecuta si no es una alerta propia.
     */
    public void showUser() {
        if(myAlertUser.getUid().contains("facebook") || myAlertUser.getUid().contains("google")) {
            ic.setImageToView(myAlertUser.getImage64(), image_user_picture);
        } else {
            if (myAlertUser.getImage64().equals("image not set")) {
                String url = "https://secure.gravatar.com/avatar/" + myAlertUser.getUid() + "?d=retro";
                ic.setImageToView(url, image_user_picture);
            } else {
                ic.setImageToView(ic.toByteArray(myAlertUser.getImage64()), image_user_picture);
            }
        }
        text_user_email.setText(myAlertUser.getEmail());
        text_user_name.setText(myAlertUser.getName());
        text_user_email.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_mail_outline_grey_500_18dp, 0,0,0);
        if(!myAlertUser.getPhone().equals("phone not set")) {
            text_user_phone.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_local_phone_grey_500_18dp,0,0,0);
            text_user_phone.setText(myAlertUser.getPhone());
        }
    }

    /**
     * Realiza la accion de salvar la informacion de la alerta en Firebase
     */
    public void saveAlertToFirebase() {
        Firebase mAlertRef = firebase.child("alerts").child(myAlert.getIdAlert());
        mAlertRef.child("reported").setValue(myAlert.getReported());
        mAlertRef.child("state").setValue(myAlert.getState());
    }

    /**
     * Genera un chooser para escoger el metodo para compartir una alerta.
     * @param view
     */
    public void shareAlert(View view) {
        String title = "¡ALERTA! Ayudame a encontrar lo que perdi. ";
        String body = "Informacion de la alerta:\nTitulo: "+ myAlert.getTitleAlert()+ "\nDescripcion: " + myAlert.getDescriptionAlert();
        if(!myAlert.getRewardAlert().equals("0")) {
            body += "\nRecompensa: " + myAlert.getRewardAlert();
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, title+body);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }


    /**
     * Suma un voto de denuncia a la alerta.
     * @param view
     */
    public void reportAlert(View view) {
        Toast.makeText(this, "Has reportado esta alerta exitosamente.", Toast.LENGTH_SHORT).show();
        if(myAlert.getReported() == LIMITE_DENUNCIAS) {
            new MaterialDialog.Builder(this).title("Denuncia de alerta")
                    .content("Ya estamos trabajando en verificar la validez de esta alerta. Vuelve de nuevo ms tarde para ver el resultado.")
                    .positiveText("OK").show();
        } else {
            myAlert.setReported(myAlert.getReported()+1);
            if (myAlert.getReported() == LIMITE_DENUNCIAS)
                myAlert.setState(1);
            reportAlert.setVisibility(View.GONE);
            new MaterialDialog.Builder(this).title("Denuncia de alerta")
                    .content("Denuncia recibida. Vuelve de nuevo ms tarde para ver el resultado.")
                    .positiveText("OK").show();
        }
        saveAlertToFirebase();
        showAlertInformation();
    }

    /**
     * Accion que realiza el boton de contactar usuario. Permite enviar un correo a la persona que creo la alerta.
     * @param view
     */
    public void contactUser(View view) {
        String subject = "¡Encontre lo que perdiste!";
        String body = "Te contacto para ponernos de acuerdo en como devolvertelo. Email: " + firebase.getAuth().getProviderData().get("email");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{myAlertUser.getEmail()});
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT   , body);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(AlertReceivedDetailActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Despliega un Dialogo que muestra el mapa y un marcador de la ubicacion donde se genero de una alerta.
     *
     * @param view
     */
    public void viewAlertMap(View view) {
        new MaterialDialog.Builder(this).title("Mapa")
                .customView(viewMap, true)
        .positiveText("OK").show();
    }

    public void viewImage(View view) {
        ImageView clicked = (ImageView) view;
        if (clicked.getDrawable()!= null) {
            if(alertImages.contains(clicked)) {
                int i = alertImages.indexOf(clicked);
                imageView.setImageDrawable(clicked.getDrawable());
            }
            new MaterialDialog.Builder(this).title("Detalle de la imagen")
                    .customView(viewImage, true)
                    .positiveText("OK").show();
        } else {
            new MaterialDialog.Builder(this).title("Detalle de la imagen")
                    .content("No hay ninguna imagen en este espacio.")
                    .positiveText("OK").show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
        mPreferences.getInstance().writePreference("receivedAlert", "empty");
    }
}
