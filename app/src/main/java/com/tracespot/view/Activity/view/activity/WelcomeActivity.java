package com.tracespot.view.Activity.view.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.auth.core.AuthProviderType;
import com.firebase.ui.auth.core.FirebaseLoginBaseActivity;
import com.firebase.ui.auth.core.FirebaseLoginError;
import com.tracespot.R;
import com.tracespot.view.Activity.model.User;

public class WelcomeActivity extends FirebaseLoginBaseActivity {
    private Firebase firebase;
    private Button button_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Firebase.setAndroidContext(this);
        firebase= new Firebase(getString(R.string.firebase_url));
        if (firebase.getAuth() != null) {
            to_App();
        }
        button_login = (Button) findViewById(R.id.button_login);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // All providers are optional! Remove any you don't want.
        setEnabledAuthProvider(AuthProviderType.FACEBOOK);
        setEnabledAuthProvider(AuthProviderType.GOOGLE);
        setEnabledAuthProvider(AuthProviderType.PASSWORD);
    }

    @Override
    protected Firebase getFirebaseRef() {
        return firebase;
    }

    @Override
    protected void onFirebaseLoginProviderError(FirebaseLoginError firebaseLoginError) {
        ShowMsgDialog(getString(R.string.login_error), getString(R.string.cant_connect_to_app));
    }

    @Override
    protected void onFirebaseLoginUserError(FirebaseLoginError firebaseLoginError) {
        System.out.println(firebaseLoginError.error);
        ShowMsgDialog(getString(R.string.login_error), firebaseLoginError.message);
    }

    @Override
    public void onFirebaseLoggedIn(final AuthData authData) {
        Firebase ref_users = firebase.child("users");
        final User user = new User();
        switch (authData.getProvider()) {
            case "google" :
                ref_users.child(authData.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.getValue() == null) {
                            user.setUid(authData.getUid());
                            user.setName(authData.getProviderData().get("displayName").toString());
                            user.setEmail(authData.getProviderData().get("email").toString());
                            user.setImage64(authData.getProviderData().get("profileImageURL").toString());
                            register_social(user);
                        } else {
                            to_App();
                        }
                    }
                    @Override
                    public void onCancelled(FirebaseError arg0) {
                    }
                });
                break;
            case "facebook":
                ref_users.child(authData.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.getValue() == null) {
                            user.setUid(authData.getUid());
                            user.setName(authData.getProviderData().get("displayName").toString());
                            user.setImage64(authData.getProviderData().get("profileImageURL").toString());
                            register_social(user);
                        } else {
                            to_App();
                        }
                    }
                    @Override
                    public void onCancelled(FirebaseError arg0) {
                    }
                });
                break;
            case "password":
                ref_users.child(authData.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.getValue() == null) {
                            user.setUid(authData.getUid());
                            user.setEmail(authData.getProviderData().get("email").toString());
                            register_social(user);
                        } else {
                            to_App();
                        }
                    }
                    @Override
                    public void onCancelled(FirebaseError arg0) {
                    }
                });
                break;
            default:
                ShowMsgDialog(getString(R.string.invalid_session), getString(R.string.not_account));
                break;

        }
    }

    @Override
    public void onFirebaseLoggedOut() {
        Toast.makeText(this, getString(R.string.closed_session), Toast.LENGTH_SHORT);
    }

    public void to_login(View view ) {
        showFirebaseLoginPrompt();
    }

    public void register_social(User user) {
        Intent i = new Intent(this, RegisterActivity.class);
        i.putExtra("user", user);
        startActivity(i);
    }

    public void to_register(View view) {
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }

    private void to_App() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }


    private void ShowMsgDialog(String type, String message) {
        new MaterialDialog.Builder(this)
                .title(type)
                .content(message)
                .positiveText(android.R.string.ok)
                .iconRes(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
