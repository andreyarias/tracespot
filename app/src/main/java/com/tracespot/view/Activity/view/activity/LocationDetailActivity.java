package com.tracespot.view.Activity.view.activity;

import android.location.Location;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.MyLocationProvider;
import com.tracespot.view.Activity.model.SearchCategory;
import com.tracespot.view.Activity.model.UserLocation;
import com.tracespot.view.Activity.model.mPreferences;

import java.util.HashMap;
import java.util.Map;

public class LocationDetailActivity extends AppCompatActivity implements GoogleMap.OnCameraChangeListener, GeoQueryEventListener {

    private GeoLocation center;
    private static final int INITIAL_ZOOM_LEVEL = 14;
    private GoogleMap map;
    private MapView mapView;
    private Circle mSearchCircle;
    private MyLocationProvider locationProvider;
    private GeoQuery geoQuery;
    private Firebase firebase;
    private GeoFire geoFire;
    private MarkerOptions marker;
    private LatLng myLatlng;

    private Toolbar toolbar;
    private TextView textview_location_name;
    private CheckBox checkbox_pet;
    private CheckBox checkbox_person;
    private CheckBox checkbox_object;
    private Button button_enable_disable;
    private UserLocation location = new UserLocation();

    private Firebase firebase_reference;
    private String ui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar_layout);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebase.setAndroidContext(this);
        firebase_reference = new Firebase(getString(R.string.firebase_url));
        ui = firebase_reference.getAuth().getUid();


        textview_location_name = (TextView) findViewById(R.id.text_detail_location_name);
        checkbox_pet = (CheckBox) findViewById(R.id.checkbox_pet_detail);
        checkbox_person = (CheckBox) findViewById(R.id.checkbox_person_detail);
        checkbox_object = (CheckBox) findViewById(R.id.checkbox_object_detail);
        button_enable_disable = (Button) findViewById(R.id.button_enable_disable_location);

        location = (UserLocation) getIntent().getSerializableExtra("userLocation");

        center = new GeoLocation(location.getLatitude(), location.getLongitude());


        firebase = firebase_reference.child("locations");
        geoFire = new GeoFire(firebase);
        geoQuery = geoFire.queryAtLocation(center, 0.5);

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {

        }
        map = mapView.getMap();
        LatLng latlngCenter = new LatLng(center.latitude, center.longitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngCenter, INITIAL_ZOOM_LEVEL));

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                setLocation(marker);
            }
        });

        textview_location_name.setText(location.getNameLocation());

        validateCheckBoxes(location);
        validateActive();

        handleNewLocation_1();
        Toast.makeText(LocationDetailActivity.this, R.string.text_location_detail, Toast.LENGTH_LONG).show();

    }

    public void validateActive(){
        if(this.location.getActive()){
            button_enable_disable.setText("Desactivar ubicación");
        } else {
            button_enable_disable.setText("Reactivar la ubicación");
        }
    }


    public void validateCheckBoxes(UserLocation location){
        checkbox_pet.setChecked(false);
        checkbox_person.setChecked(false);
        checkbox_object.setChecked(false);
        for (SearchCategory category: location.getCategories()) {
            if(category.getId() == 1){
                checkbox_pet.setChecked(true);
            }
            if(category.getId() == 2){
                checkbox_person.setChecked(true);
            }
            if(category.getId() == 3){
                checkbox_object.setChecked(true);
            }
        }
    }

    public void deleteLocation(View view){
        firebase_reference.child("locations").child(location.getId()).removeValue();
        firebase_reference.child("userLocations").child(ui).child(location.getId()).removeValue();
        firebase_reference.child("locationCategories").child(location.getId()).removeValue();
        firebase_reference.child("alertsReceived").child(location.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap: dataSnapshot.getChildren()) {
                    firebase_reference.child("alertsReceived").child(location.getId()).child(snap.getKey()).removeValue();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        Toast.makeText(LocationDetailActivity.this, "Ubicación eliminada con éxito", Toast.LENGTH_SHORT).show();
        mPreferences.getInstance().setUserHasLocation(false);
        this.finish();
    }

    public void enable_disable_Location(View view){
        if(location.getActive()){
            location.setActive(false);
            firebase_reference.child("locations").child(location.getId()).child("active").setValue(false);
            Toast.makeText(LocationDetailActivity.this, "La ubicación se ha desactivado", Toast.LENGTH_SHORT).show();
        } else {
            location.setActive(false);
            firebase_reference.child("locations").child(location.getId()).child("active").setValue(true);
            Toast.makeText(LocationDetailActivity.this, "La ubicación se ha reactivado", Toast.LENGTH_SHORT).show();
        }
        this.finish();
    }

    public void modify_Location(View view) {

        String nameLocation = textview_location_name.getText().toString();

        if (myLatlng == null) {
            new MaterialDialog.Builder(LocationDetailActivity.this)
                    .content(getString(R.string.not_location_yet))
                    .positiveText(getString(R.string.gotit))
                    .show();
        } else {
            if (!nameLocation.equals("")) {
               // Firebase firebase = new Firebase("https://dev-ts.firebaseio.com/");
                Firebase locations = firebase_reference.child("locations").child(location.getId());//.child(nameLocation);
                Firebase categories = firebase_reference.child("locationCategories").child(location.getId());//.child(nameLocation);

                locations.child("nameLocation").setValue(nameLocation);
                Firebase mapRefLatLong = locations.child("l");

                geoFire = new GeoFire(locations);


                location.setLatitude(myLatlng.latitude);
                location.setLongitude(myLatlng.longitude);
                location.setActive(true);


                Map<String, Object> mapLocationsLatLong = new HashMap<String, Object>();
                Map<String, Object> mapLocations = new HashMap<String, Object>();

                if (checkbox_pet.isChecked()) {
                    mapLocations.put("1", true);
                }
                if (checkbox_person.isChecked()) {
                    mapLocations.put("2", true);
                }
                if (checkbox_object.isChecked()) {
                    mapLocations.put("3", true);
                }

                if (mapLocations.size() == 0) {
                    new MaterialDialog.Builder(LocationDetailActivity.this)
                            .content(R.string.content)
                            .positiveText(getString(R.string.gotit))
                            .show();
                } else {
                    mapLocationsLatLong.put("0",location.getLatitude());
                    mapLocationsLatLong.put("1",location.getLongitude());

                    mapRefLatLong.setValue(mapLocationsLatLong);
                    categories.setValue(mapLocations);



                    textview_location_name.setText("");
                    checkbox_person.setChecked(false);
                    checkbox_pet.setChecked(false);
                    checkbox_object.setChecked(false);
                    Toast.makeText(LocationDetailActivity.this, getString(R.string.location_updated), Toast.LENGTH_LONG).show();
                    finish();
                }


            } else {
                new MaterialDialog.Builder(LocationDetailActivity.this)
                        .content(getString(R.string.location_must_have_name))
                        .positiveText(getString(R.string.gotit))
                        .show();
            }
        }
    }

    public void handleNewLocation_1() {
        center = new GeoLocation(location.getLatitude(), location.getLongitude());
        LatLng latlng = new LatLng(center.latitude, center.longitude);


        marker = new MarkerOptions().position(latlng)
                .title(getString(R.string.title_marker))
                .draggable(true);

        myLatlng = marker.getPosition();


        map.addMarker(marker);


        map.moveCamera(CameraUpdateFactory.newLatLng(latlng));


        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, INITIAL_ZOOM_LEVEL));

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        LatLng newCenter = cameraPosition.target;
        geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
    }

    public void setLocation(Marker marker) {
        myLatlng = marker.getPosition();
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {

    }

    @Override
    public void onKeyExited(String key) {

    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(FirebaseError error) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}





