package com.tracespot.view.Activity.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.Firebase;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.google.android.gms.maps.model.LatLng;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.AlertsReceivedController;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.Alert;
import com.tracespot.view.Activity.model.SpinnerItem;
import com.tracespot.view.Activity.view.adapter.SpinnerAdapter;

import net.yazeed44.imagepicker.util.Picker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class GenerateAlertActivity extends AppCompatActivity {

    private EditText titleAlert;
    private EditText descriptionAlert;
    private EditText rewardAlert;
    private Spinner typeAlert;
    private Button createAlert;
    private Firebase firebase;
    private Firebase locationsRef;
    private GeoFire geoFire;
    private GeoLocation center;
    private GeoQuery geoQuery;
    private LatLng myLatlng;
    private int rangeAlert;
    private ImageView active;
    private ArrayList<ImageView> alertImages;
    private ImageController ic;
    private Alert alert;
    private AlertsReceivedController alertsReceivedController;

    Map<String, Object> mapLocations;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_alert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.create_alert));
        alert = new Alert();
        myLatlng = new LatLng((double) getIntent().getSerializableExtra("latitude"), (double) getIntent().getSerializableExtra("longitude"));
        Firebase.setAndroidContext(this);
        firebase = new Firebase(getString(R.string.firebase_url));
        mapLocations = new HashMap<String, Object>();
        typeAlert = (Spinner) findViewById(R.id.textview_alert_type);
        createAlert = (Button) findViewById(R.id.createAlert);
        titleAlert = (EditText) findViewById(R.id.editAlertTitle);
        descriptionAlert = (EditText) findViewById(R.id.editAlertDescription);
        rewardAlert = (EditText) findViewById(R.id.editAlertReward);
        SpinnerItem arrayTypesAlert[] = new SpinnerItem[3];
        arrayTypesAlert[0] = new SpinnerItem("Mascotas", R.drawable.ic_pets);
        arrayTypesAlert[1] = new SpinnerItem("Personas", R.drawable.ic_persons);
        arrayTypesAlert[2] = new SpinnerItem("Objetos", R.drawable.ic_other_objects);

        //Obtener los image views del layout y agregarlos a la lista
        alertImages = new ArrayList<>();
        alertImages.add((ImageView) findViewById(R.id.image_alertFront));
        alertImages.add((ImageView) findViewById(R.id.image_alertSide));
        alertImages.add((ImageView) findViewById(R.id.image_alertBack));

        active = (ImageView) findViewById(R.id.image_alertActive);

        //Inicializar el controlador de imagenes
        ic = new ImageController(3, Picker.PickMode.MULTIPLE_IMAGES);

        //Agregar los views al controlador para refrescarlos dinamicamente
        for (ImageView iv:alertImages) {
            ic.addView(iv);
        }

//        alert.setCategory(1);
//        alert.setRange(2);
//        rangeAlert = 2;

        SpinnerAdapter adapter = new SpinnerAdapter(this, arrayTypesAlert);
        typeAlert.setAdapter(adapter);

        typeAlert.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mapLocations.clear();
                switch (position) {
                    case 0:
                        alert.setCategory(1);
                        alert.setRange(2);
                        rangeAlert = 2;
                        break;
                    case 1:
                        alert.setCategory(2);
                        alert.setRange(50);
                        rangeAlert = 50;
                        break;
                    case 2:
                        alert.setCategory(3);
                        alert.setRange(2);
                        rangeAlert = 2;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        alertsReceivedController = new AlertsReceivedController(myLatlng.latitude, myLatlng.longitude, rangeAlert, alert.getCategory());
    }

    public void create_alert(View view) {
        String uid = firebase.getAuth().getUid();
        if (titleAlert.getText().toString().isEmpty()) {
            new MaterialDialog.Builder(GenerateAlertActivity.this)
                    .content(getString(R.string.alert_must_have_name))
                    .positiveText(getString(R.string.gotit))
                    .show();
        } else {
            if (descriptionAlert.getText().toString().isEmpty()) {
                new MaterialDialog.Builder(GenerateAlertActivity.this)
                        .content(getString(R.string.alert_must_have_description))
                        .positiveText(getString(R.string.gotit))
                        .show();
            } else {

                alert.setOwner(uid);
                alert.setTitleAlert(titleAlert.getText().toString().trim());
                alert.setDescriptionAlert(descriptionAlert.getText().toString().trim());
                alert.setRewardAlert(rewardAlert.getText().toString().trim());
                alert.setLatitude(myLatlng.latitude);
                alert.setLongitude(myLatlng.longitude);
//                alert.setImages(ic.getAsBase64());
//                alert.setRange(rangeAlert);
                alert.setState(0);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTimeStamp = dateFormat.format(new Date()); // Find todays date
                alert.setCreatedAt(currentTimeStamp);
                Firebase newRef = firebase.child("alerts").push();
                newRef.setValue(alert);
//                newRef.child("categories").updateChildren(mapLocations);

                Firebase imagesRef = firebase.child("images").child(newRef.getKey());
                imagesRef.setValue(ic.getAsBase64());

                Firebase alertsSentRef = firebase.child("alertsSent");

                alertsSentRef.child(uid).child(newRef.getKey()).setValue("true");
                alertsReceivedController = new AlertsReceivedController(myLatlng.latitude, myLatlng.longitude, rangeAlert, alert.getCategory(), newRef.getKey(), uid);
                finish();
            }
        }
    }

    public void add_alert_image (View view) {
        if(count() < 3) {
            ic.setMyContext(view.getContext());
            ic.selectImage();
        }
        if(count() == 3) {
            Toast.makeText(GenerateAlertActivity.this, "Ya has seleccionado las tres imagenes.", Toast.LENGTH_LONG).show();
        }
    }

    public void change_active (View view) {
        ImageView iv =(ImageView) view;
        if (iv.getDrawable() != null) {
            setActive(iv);
        } else {
            add_alert_image(view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshActive();
    }

    public void refreshActive() {
        int selected = count();
        if(selected > 0) {
            setActive(alertImages.get(selected-1));
        } else {
            active.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_box_grey_500_24dp));
        }
    }

    private void setActive(ImageView iv) {
        active.setImageDrawable(iv.getDrawable());
    }

    private int count() {
        int filled=0;
        for (ImageView iv :
                alertImages) {
            if (iv.getDrawable()!=null) {
                filled++;
            }
        }
        return filled;
    }

    @Override
    protected void onStart() {
        super.onStart();
//        alertsReceivedController.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        alertsReceivedController.onStop();
    }

}
