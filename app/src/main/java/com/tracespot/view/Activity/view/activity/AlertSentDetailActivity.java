package com.tracespot.view.Activity.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.Firebase;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ImageController;
import com.tracespot.view.Activity.model.Alert;
import com.tracespot.view.Activity.model.mPreferences;

import net.yazeed44.imagepicker.util.Picker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AlertSentDetailActivity extends AppCompatActivity {

    public final static int ACTIVE_STATE = 0;
    public final static int SUSPENDED_STATE = 1;
    public final static int END_STATE = 2;
    public final static int DELETED_STATE = 3;//NO se ha implementado

    //CardView alert_info
    private EditText titleAlert;
    private EditText descriptionAlert;
    private EditText rewardAlert;
    private EditText stateAlert;
    private TextView typeAlert;

    //CardView alert_images
    private ImageView active;
    private ArrayList<ImageView> alertImages;


    //CardView alert_actions
    private FloatingActionButton editAlert;
    private FloatingActionButton saveAlert;
    private FloatingActionButton deleteAlert;
    private FloatingActionButton shareAlert;
    private FloatingActionButton endAlert;

    //Models and Controllers
    private ImageController ic;
    private Alert myAlert;

    //Firebase objects
    private Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_sent_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_layout);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //inicializacion de los componentes
        initializeComponents();
        setAlert();

    }

    private void initializeComponents() {
        //Inicializacion de Firebase
        firebase = new Firebase(getString(R.string.firebase_url));

        //Inicializacion de componentes botones
        editAlert = (FloatingActionButton) findViewById(R.id.button_edit_alert);
        saveAlert = (FloatingActionButton) findViewById(R.id.button_save_alert);
        endAlert = (FloatingActionButton) findViewById(R.id.button_end_alert);
        deleteAlert = (FloatingActionButton) findViewById(R.id.button_delete_alert);
        shareAlert = (FloatingActionButton) findViewById(R.id.button_share_alert);

        //Inicializacion de componentes de alerta
        titleAlert = (EditText) findViewById(R.id.txt_alert_title);
        descriptionAlert = (EditText) findViewById(R.id.txt_alert_description);
        rewardAlert = (EditText) findViewById(R.id.txt_alert_reward);
        stateAlert = (EditText) findViewById(R.id.txt_alert_state);
        typeAlert = (TextView) findViewById(R.id.textview_alert_type);

        //Obtener los image views del layout y agregarlos a la lista
        alertImages = new ArrayList<>();
        alertImages.add((ImageView) findViewById(R.id.image_alertFront));
        alertImages.add((ImageView) findViewById(R.id.image_alertSide));
        alertImages.add((ImageView) findViewById(R.id.image_alertBack));
        //Inicializar la vista grande de las imagenes.
        active = (ImageView) findViewById(R.id.image_alertActive);

        //Inicializar el controlador de imagenes
        ic = new ImageController(this, 3, Picker.PickMode.MULTIPLE_IMAGES);

        //Agregar los views al controlador para refrescarlos dinamicamente
        for (ImageView iv:alertImages) {
            ic.addView(iv);
        }
    }

    private void setAlert() {
        myAlert = mPreferences.getInstance().readAlertStored("sentAlert", 0);
        if(myAlert != null) {
            showAlertInformation();
        } else {
            finish();
        }
    }

    /**
     * Muestra la informacion de una alerta, es decir, el detalle de la misma.
     */
    public void showAlertInformation() {
        titleAlert.setText(myAlert.getTitleAlert());
        descriptionAlert.setText(myAlert.getDescriptionAlert());
        rewardAlert.setText(myAlert.getRewardAlert());
        if (myAlert.getState() == ACTIVE_STATE) {
            stateAlert.setText(R.string.alert_state_active);
        } else {
            if (myAlert.getState() == SUSPENDED_STATE)  {
                stateAlert.setText(R.string.alert_state_suspended);
            } else if(myAlert.getState() == END_STATE){
                stateAlert.setText(R.string.alert_state_finished);
            } else {
                stateAlert.setText(R.string.alert_state_deleted);
            }
            FloatingActionButton btnEndAlert = (FloatingActionButton) findViewById(R.id.button_end_alert);
            btnEndAlert.setVisibility(View.GONE);
            editAlert.setVisibility(View.GONE);
            saveAlert.setVisibility(View.GONE);
        }
        for (int i = 0; i < myAlert.getImages().size(); i++) {
            ic.setImageToView(ic.toByteArray(myAlert.getImages().get(i)), alertImages.get(i));
            setActive(alertImages.get(i));
        }
        switch (myAlert.getCategory()) {
            case 1:
                typeAlert.setText(getString(R.string.checklist_pet));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_pets, 0, 0, 0);
                break;
            case 2:
                typeAlert.setText(getString(R.string.checklist_person));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_persons, 0, 0, 0);
                break;
            case 3:
                typeAlert.setText(getString(R.string.checklist_object));
                typeAlert.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_other_objects, 0, 0, 0);
                break;
            default:
                break;
        }
    }

    /**
     * Habilita los cambios de edicion.
     */
    public void enableFields() {
        titleAlert.setEnabled(true);
        descriptionAlert.setEnabled(true);
        rewardAlert.setEnabled(true);
        typeAlert.setEnabled(true);
    }

    /**
     * Deshabilita los campos de edicion.
     */
    public void disableFields() {
        titleAlert.setEnabled(false);
        descriptionAlert.setEnabled(false);
        rewardAlert.setEnabled(false);
        typeAlert.setEnabled(false);
    }

    /**
     * Realiza la accion de salvar la informacion de la alerta en Firebase
     */
    public void saveAlertToFirebase() {
        if(!myAlert.getTitleAlert().equals(titleAlert.getText())) {
           myAlert.setTitleAlert(titleAlert.getText().toString());
        }
        if(!myAlert.getDescriptionAlert().equals(descriptionAlert.getText())) {
            myAlert.setDescriptionAlert(descriptionAlert.getText().toString());
        }
        if(!myAlert.getRewardAlert().equals(rewardAlert.getText())) {
            myAlert.setRewardAlert(rewardAlert.getText().toString());
        }
        Firebase mAlertRef = firebase.child("alerts").child(myAlert.getIdAlert());
        mAlertRef.child("titleAlert").setValue(myAlert.getTitleAlert());
        mAlertRef.child("descriptionAlert").setValue(myAlert.getDescriptionAlert());
        mAlertRef.child("rewardAlert").setValue(myAlert.getRewardAlert());
        mAlertRef.child("state").setValue(myAlert.getState());
        if (myAlert.getState() == 2) {
            mAlertRef.child("finishedAt").setValue(myAlert.getFinishedAt());
        }
    }

    private void saveAlertImagesToFirebase() {
        //Proceso para guardar las imagenes
        ArrayList<String> mImages = ic.getAsBase64();
        if (mImages != null) {
            refreshActive();
            myAlert.setImages(mImages);
            Firebase mAlertImagesRef = firebase.child("images").child(myAlert.getIdAlert());
            for (int i = 0; i < myAlert.getImages().size(); i++) {
                mAlertImagesRef.child(""+i).setValue(myAlert.getImages().get(i));
            }
        }
    }

    /**
     * Cambia el estado de una alerta a finalizada. No puede ser deshecho.
     * @param view
     */
    public void endAlert(View view) {
        new MaterialDialog.Builder(this)
                .content("¿Ya alguien te ayudo a encontrar lo que perdiste? Esta accion no puede ser deshecha.")
                .positiveText("Confirmar")
                .negativeText("Cancelar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        myAlert.setState(END_STATE);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String currentTimeStamp = dateFormat.format(new Date()); // Find todays date
                        myAlert.setFinishedAt(currentTimeStamp);
                        showAlertInformation();
                        saveAlertToFirebase();
                        Toast.makeText(AlertSentDetailActivity.this, "Alerta finalizada", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .show();
    }

    /**
     * Elimina una alerta de Firebase.
     * @param view
     */
    public void deleteAlert(View view) {
        new MaterialDialog.Builder(this)
                .content("¿Esta seguro que desea eliminar esta alerta? Esta accion no puede ser deshecha.")
                .positiveText("Eliminar")
                .negativeText("Cancelar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        myAlert.setState(DELETED_STATE);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String currentTimeStamp = dateFormat.format(new Date()); // Find todays date
                        myAlert.setFinishedAt(currentTimeStamp);
                        showAlertInformation();
                        saveAlertToFirebase();
                        Toast.makeText(AlertSentDetailActivity.this, "Alerta Eliminada", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .show();
    }

    /**
     * Habilita los campos para la edicion de la informacion de la alerta.
     * @param view
     */
    public void editAlert(View view) {
        enableFields();
        saveAlert.setVisibility(View.VISIBLE);
        editAlert.setVisibility(View.GONE);
    }

    /**
     * Guarda los cambios hechos a una alerta en Firebase y muestra de nuevo la informacion.
     * @param view
     */
    public void saveAlert(View view) {
        disableFields();
        saveAlertToFirebase();
        showAlertInformation();
        saveAlert.setVisibility(View.GONE);
        editAlert.setVisibility(View.VISIBLE);
    }

    /**
     * Genera un chooser para escoger el metodo para compartir una alerta.
     * @param view
     */
    public void shareAlert(View view) {
        String title = "¡ALERTA! Ayudame a encontrar lo que perdi. ";
        String body = "Informacion de la alerta:\nTitulo: "+ myAlert.getTitleAlert()+ "\nDescripcion: " + myAlert.getDescriptionAlert();
        if(!myAlert.getRewardAlert().equals("0")) {
            body += "\nRecompensa: " + myAlert.getRewardAlert();
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, title+body);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    //Image Control actions below
    /**
     * Metodo para cambiar la imagen activa, es decir la imagen que se muestra mas grande.
     * @param view
     */
    public void change_active (View view) {
        ImageView iv =(ImageView) view;
        if (iv.getDrawable() != null) {
            setActive(iv);
        } else {
            add_alert_image(view);
        }
    }

    public void add_alert_image (View view) {
        if(count() < 3) {
            ic.setMyContext(view.getContext());
            ic.selectImage();
        }
        if(count() == 3) {
            Toast.makeText(AlertSentDetailActivity.this, "Ya has seleccionado las tres imagenes.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        saveAlertImagesToFirebase();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPreferences.getInstance().writePreference("sentAlert", "empty");
    }

    public void refreshActive() {
        int selected = count();
        if(selected > 0) {
            setActive(alertImages.get(selected-1));
        } else {
            active.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_box_grey_500_24dp));
        }
    }

    private void setActive(ImageView iv) {
        active.setImageDrawable(iv.getDrawable());
    }

    private int count() {
        int filled=0;
        for (ImageView iv :
                alertImages) {
            if (iv.getDrawable()!=null) {
                filled++;
            }
        }
        return filled;
    }

}
