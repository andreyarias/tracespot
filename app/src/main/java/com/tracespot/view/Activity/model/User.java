package com.tracespot.view.Activity.model;

import java.io.Serializable;

/**
 * Created by tracespot on 4/16/2016.
 */
public class User implements Serializable{
    private String uid;
    private String email;
    private String name;
    private int age;
    private String image64;
    private String phone;
    private int helped;
    private int aided;



    public User() {
        uid = "uid not set";
        email="email not set";
        name="name not set";
        age=0;
        image64="image not set";
        phone="phone not set";
        helped = 0;
        aided = 0;

    }

    public User(String email, String name,int age, String image64, String phone) {
        this();
        setEmail(email);
        setName(name);
        setAge(age);
        setImage64(image64);
        setPhone(phone);
    }

    public User(String uid, String email, String name,int age) {
        this();
        setEmail(email);
        setName(name);
        setAge(age);
    }

    public User(String uid, String email, String name,int age, String image64, String phone) {
        this();
        setEmail(email);
        setName(name);
        setAge(age);
        setImage64(image64);
        setPhone(phone);
        setUid(uid);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getImage64() {
        return image64;
    }

    public void setImage64(String image64) {
        this.image64 = image64;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getHelped() {
        return helped;
    }

    public void setHelped(int helped) {
        this.helped = helped;
    }

    public int getAided() {
        return aided;
    }

    public void setAided(int aided) {
        this.aided = aided;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}

