package com.tracespot.view.Activity.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tracespot.view.Activity.view.fragments.AlertsSentFragment;
import com.tracespot.view.Activity.view.fragments.GenerateAlertFragment;
import com.tracespot.view.Activity.view.fragments.PlacesFragment;
import com.tracespot.view.Activity.view.fragments.ProfileFragment;
import com.tracespot.view.Activity.view.fragments.ReceivedAlertsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tracespot on 21/04/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> fragments;
    int numTabs;

    public ViewPagerAdapter(FragmentManager fm, int numTabs) {
        super(fm);
        fragments = new ArrayList<>();
        this.numTabs = numTabs;
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
//        return fragments.get(position);
        switch (position) {
            case 0:
                ReceivedAlertsFragment tab1 = new ReceivedAlertsFragment();
                return tab1;
            case 1:
                AlertsSentFragment tab2 = new AlertsSentFragment();
                return tab2;
            case 2:
                GenerateAlertFragment tab3 = new GenerateAlertFragment();
                return tab3;
            case 3:
                PlacesFragment tab4 = new PlacesFragment();
                return tab4;
            case 4:
                ProfileFragment tab5 = new ProfileFragment();
                return tab5;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
//        return 5;
        return numTabs;
    }
}