package com.tracespot.view.Activity.model;

/**
 * Created by tracespot on 11/06/2016.
 */
public class AlertReceived extends Alert{

    private UserLocation location;
    private User user;

    public AlertReceived(){
        super();
        this.location = new UserLocation();
        this.user = new User();
    }


    // Sets

    public void setLocation(UserLocation location){
        this.location = location;
    }

    public void setUser(User user){
        this.user = user;
    }

    //Gets

    public UserLocation getLocation(){
        return this.location;
    }

    public User getUser(){
        return this.user;
    }
}
