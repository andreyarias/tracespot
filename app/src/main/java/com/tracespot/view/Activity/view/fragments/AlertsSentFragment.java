package com.tracespot.view.Activity.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.google.gson.Gson;
import com.tracespot.R;
import com.tracespot.view.Activity.model.Alert;
import com.tracespot.view.Activity.model.mPreferences;
import com.tracespot.view.Activity.view.activity.AlertReceivedDetailActivity;
import com.tracespot.view.Activity.view.activity.AlertSentDetailActivity;
import com.tracespot.view.Activity.view.adapter.AlertsSentListAdapter;

import java.util.ArrayList;

/**
 * Created by tracespot on 25/04/16.
 */
public class AlertsSentFragment extends BaseFragment {

    private AlertsSentListAdapter adapter;
    private View rootView;

    private Firebase firebase_reference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        this.rootView = rootView;

        Context context = getActivity();

        int idLayout = R.layout.fragment_view_alert_sent_in_list;

        this.adapter = new AlertsSentListAdapter(idLayout,context);

        final ListView list_view = (ListView) rootView.findViewById(R.id.list_alerts_sent);
        list_view.setAdapter(adapter);

        registerClickCallback(rootView);

        return rootView;
    }



    private void registerClickCallback(View view){
        ListView list = (ListView) view.findViewById(R.id.list_alerts_sent);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked , int position, long id) {
                Alert alert = adapter.getAlerts().get(position);
                Intent new_activity = new Intent(getActivity(), AlertSentDetailActivity.class);
                mPreferences.getInstance().writePreference("sentAlert", alert);
                getActivity().startActivity(new_activity);
            }
        });
    }

    @Override
    protected int getLayoutFromResource() {
        return R.layout.fragment_alert_sent;
    }
}
