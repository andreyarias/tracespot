package com.tracespot.view.Activity.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tracespot.R;
import com.tracespot.view.Activity.model.SpinnerItem;

/**
 * Created by tracespot on 26/05/16.
 */
public class SpinnerAdapter extends BaseAdapter {

    Context context;

    /**
     * variable to store the list of items of type Spinner
     */
    SpinnerItem[] items;

    /**
     * method constructor to get the context and the items of the spinner
     * @param c
     *         context from the parent
     * @param items
     *          list of items type spinner
     */
    public SpinnerAdapter(Context c, SpinnerItem[] items) {
        this.context = c;
        this.items = items;
    }

    /**
     * method to get the size of the list of items
     * @return
     */
    @Override
    public int getCount() {
        return items.length;
    }

    /**
     * method to get an item from the list of items
     * @param i
     *          position of the item in the list
     * @return
     */
    @Override
    public Object getItem(int i) {
        return items[i];
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     * method to create and get the view of an object for the spinner
     * @param i
     *          position of the item in the list
     * @param view
     * @param viewGroup
     * @return
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SpinnerItem item = (SpinnerItem) getItem(i);
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.spinner_item, viewGroup, false);
        TextView spinnerTextView = (TextView) row.findViewById(R.id.spinner_text);
        ImageView spinnerImageView = (ImageView) row.findViewById(R.id.spinner_image);
        spinnerTextView.setText(item.spinnerText);
        spinnerImageView.setImageResource(item.spinnerImageId);
        return row;
    }
}
