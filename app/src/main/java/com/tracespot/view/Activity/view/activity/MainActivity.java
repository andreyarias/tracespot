package com.tracespot.view.Activity.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.tracespot.R;
import com.tracespot.view.Activity.controller.ViewPagerAdapter;
import com.tracespot.view.Activity.model.mPreferences;
import com.tracespot.view.Activity.view.fragments.AlertsSentFragment;
import com.tracespot.view.Activity.view.fragments.GenerateAlertFragment;
import com.tracespot.view.Activity.view.fragments.PlacesFragment;
import com.tracespot.view.Activity.view.fragments.ProfileFragment;
import com.tracespot.view.Activity.view.fragments.ReceivedAlertsFragment;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private Firebase mRef;
    private MaterialDialog currentDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_alerts_received));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_detail_alert));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_generate_alert));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_places));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_profile));


        viewPager = (ViewPager) findViewById(R.id.viewpager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        createFragmentsForPager();
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setSupportActionBar(toolbar);

        tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.indicator));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.indicator));

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        Firebase.setAndroidContext(this);
        mRef = new Firebase(getString(R.string.firebase_url));
        mRef.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if(authData == null) {
                    authenticate();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_open_session), Toast.LENGTH_LONG).show();
                }
            }
        });
        mPreferences.getInstance().Initialize(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void createFragmentsForPager() {
        ReceivedAlertsFragment alertsReceivedFragment = new ReceivedAlertsFragment();
        viewPagerAdapter.addFragment(alertsReceivedFragment);

        AlertsSentFragment alertsSentFragment = new AlertsSentFragment();
        viewPagerAdapter.addFragment(alertsSentFragment);

        GenerateAlertFragment generateAlertFragment = new GenerateAlertFragment();
        viewPagerAdapter.addFragment(generateAlertFragment);

        PlacesFragment placesFragment = new PlacesFragment();
        viewPagerAdapter.addFragment(placesFragment);

        ProfileFragment profileFragment = new ProfileFragment();
        viewPagerAdapter.addFragment(profileFragment);
    }

    public void logout(View view) {
        mRef.unauth();
        authenticate();
    }

    public void authenticate() {
        Intent i = new Intent(this, WelcomeActivity.class);
        startActivity(i);
    }

    public void noLocationMessage() {
        boolean haslocation = mPreferences.getInstance().getUserHasLocation();
        if (!haslocation) {
            String uid = mRef.getAuth().getUid();
            mRef.child("userLocations").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        if (currentDialog == null) {
                                currentDialog = new MaterialDialog.Builder(MainActivity.this).title(getString(R.string.important))
                                        .content(getString(R.string.must_have_locations))
                                        .positiveText(getString(R.string.create_location))
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                startActivity(new Intent(MainActivity.this, NewPlaceActivity.class));
                                                currentDialog = null;
                                            }
                                        })
                                        .dismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                if (currentDialog != null) {
                                                    noLocationMessage();
                                                }
                                            }
                                        })
                                        .show();

                        } else {
                            if (!currentDialog.isShowing()) {
                                currentDialog = new MaterialDialog.Builder(MainActivity.this).title(getString(R.string.important))
                                        .content(getString(R.string.must_have_locations))
                                        .positiveText(getString(R.string.create_location))
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                startActivity(new Intent(MainActivity.this, NewPlaceActivity.class));
                                                currentDialog = null;
                                            }
                                        })
                                        .dismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                if (currentDialog != null) {
                                                    noLocationMessage();
                                                }
                                            }
                                        })
                                        .show();
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        noLocationMessage();
    }
}